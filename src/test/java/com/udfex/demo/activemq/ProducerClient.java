package com.udfex.demo.activemq;

import com.alibaba.fastjson.JSONObject;
import com.udfex.ucs.amq.api.*;
import org.apache.activemq.ActiveMQConnectionFactory;

import java.util.Date;
import java.util.Properties;


public class ProducerClient {

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(ProducerConst.TOPIC, "GUEST.PID_UDFEX_PRODUCER_TEST_00001");
        //properties.put(PropertyKeyConst.AccessKey, "4ZY3Hr1k3VSOi6PW");
        //properties.put(PropertyKeyConst.SecretKey, "OM44UesEeGvmxfpu3LKtb4PvzVTw55");
        SimpleTransportListener simpleTransportListener = new SimpleTransportListener();
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setTransportListener(simpleTransportListener);
        activeMQConnectionFactory.setBrokerURL("failover:(tcp://192.168.8.211:61616)");
        IAMQFactory iamqFactory = new IAMQFactory(activeMQConnectionFactory);
        Producer producer = iamqFactory.createProducer(properties);
        MockData data = new MockData();
        data.setId("000001");
        data.setAge(12);
        data.setBirthDay(new Date());
        data.setName("测试队列1");
        DataMessage dataMessage = new DataMessage();
        dataMessage.setTopic("GUEST.PID_UDFEX_PRODUCER_TEST_00002");
        dataMessage.setMessageBody(JSONObject.toJSONString(data));
        producer.send(dataMessage);


        Properties properties2 = new Properties();
        properties2.put(ProducerConst.TOPIC, "GUEST.PID_UDFEX_PRODUCER_TEST_00002");
        //properties.put(PropertyKeyConst.AccessKey, "4ZY3Hr1k3VSOi6PW");
        //properties.put(PropertyKeyConst.SecretKey, "OM44UesEeGvmxfpu3LKtb4PvzVTw55");
        activeMQConnectionFactory.setBrokerURL("failover:(tcp://192.168.8.211:61616)");
        Producer producer2 = iamqFactory.createProducer(properties2);
        MockData2 data2 = new MockData2();
        data2.setId("000002");
        data2.setAge(22);
        data2.setBirthDay(new Date());
        data2.setName("测试队列2");
        //producer2.send(JSONObject.toJSONString(data2));
    }
}
