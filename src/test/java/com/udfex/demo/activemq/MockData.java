package com.udfex.demo.activemq;

import com.udfex.ucs.amq.api.BaseMessage;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jeng on 2016/3/11.
 */
public class MockData extends BaseMessage implements Serializable {
    private String id;
    private String name;
    private Integer age;
    private Date birthDay;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }
}
