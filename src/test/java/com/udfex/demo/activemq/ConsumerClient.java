package com.udfex.demo.activemq;

import com.alibaba.fastjson.JSONObject;
import com.udfex.ucs.amq.api.*;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Properties;

/**
 * Created by Jeng on 16/1/31.
 */
public class ConsumerClient {

    public static void main(String[] args) {
        Properties properties = new Properties();
        //properties.put(PropertyKeyConst.AccessKey, "4ZY3Hr1k3VSOi6PW");
        //properties.put(PropertyKeyConst.SecretKey, "OM44UesEeGvmxfpu3LKtb4PvzVTw55");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL("failover:(tcp://192.168.8.211:61616)");
        IAMQFactory iamqFactory = new IAMQFactory(activeMQConnectionFactory);
        Consumer consumer = iamqFactory.createConsumer(properties);
        consumer.subscribe("UUUUUU1", new MessageToObject<MockData>() {
            @Override
            public MockData fromMessage(DataMessage dataMessage) {
                return JSONObject.parseObject(dataMessage.getMessageBody(), MockData.class);
            }
        }, new MessageHandler<MockData>() {
            @Override
            public void handler(MockData message) {
                System.out.println("消费,GUEST.PID_UDFEX_PRODUCER_TEST_00001：" + JSONObject.toJSONString(message));
                if(message.getAge() %2 ==0){
                    throw new IllegalArgumentException("不是偶数");
                }
            }
        });
        consumer.start();


        Consumer consumer2 = iamqFactory.createConsumer(properties);
        consumer2.subscribe("GUEST.PID_UDFEX_PRODUCER_TEST_00002", new MessageToObject<MockData2>() {
            @Override
            public MockData2 fromMessage(DataMessage dataMessage) {
                return JSONObject.parseObject(dataMessage.getMessageBody(), MockData2.class);
            }
        }, new MessageHandler<MockData2>() {
            @Override
            public void handler(MockData2 message) {
                System.out.println("消费,GUEST.PID_UDFEX_PRODUCER_TEST_00002：" + message.getName());
            }
        });
        consumer2.start();
    }
}
