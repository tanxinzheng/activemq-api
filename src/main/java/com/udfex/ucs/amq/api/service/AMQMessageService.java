package com.udfex.ucs.amq.api.service;

import java.util.List;

/**
 * Created by Jeng on 2016/1/18.
 */
public interface AMQMessageService {

    /**
     * 发送消息
     * @param destination
     * @param code
     * @param messageBody
     */
    public String sendMessage(String destination, String code, Object messageBody);

    /**
     * 重发单个消息
     * @param messageId
     */
    public void retryMessage(String messageId);

    /**
     * 批量重发
     * @param messageIds
     */
    public void retryMessageBatch(List<String> messageIds);

    /**
     * 补发消息（待投递|投递失败|投递成功且处理失败的消息）
     */
    public void retryMessageAll();

}
