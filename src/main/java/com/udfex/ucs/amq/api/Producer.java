package com.udfex.ucs.amq.api;

import com.udfex.ucs.amq.api.service.ProducerService;

/**
 * Created by Jeng on 2016/1/12.
 */
public interface Producer {

    /**
     * 发送消息
     * @param object
     * @return
     */
    public SendResult send(DataMessage dataMessage);

    /**
     * 开始连接AMQ服务
     */
    public void start();

    /**
     * 断开AMQ服务连接
     */
    public void shutdown();
}
