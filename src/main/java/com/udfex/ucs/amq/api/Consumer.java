package com.udfex.ucs.amq.api;

/**
 * Created by Jeng on 2016/1/15.
 */
public interface Consumer {

    /**
     * 消费消息
      * @param topic
     */
    public void subscribe(String topic, MessageToObject messageToObject, MessageHandler messageHandler);

    /**
     * 开始接收消息
     */
    public void start();

    /**
     * 停止接收消息
     */
    public void shutdown();
}
