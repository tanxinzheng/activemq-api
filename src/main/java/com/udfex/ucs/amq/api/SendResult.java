package com.udfex.ucs.amq.api;

/**
 * Created by Jeng on 16/1/31.
 */
public class SendResult {

    public static int SEND_SUCCESS = 1;
    public static int SEND_FAILURE = 0;

    // 0：失败，1：成功，
    private int resultCode;
    private String resultMsg;
    private String messageId;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
}
