package com.udfex.ucs.amq.api;

/**
 * Created by Jeng on 2016/3/10.
 */
public interface MessageHandler<T extends BaseMessage> {

    public void handler(T message);
}
