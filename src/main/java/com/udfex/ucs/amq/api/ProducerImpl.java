package com.udfex.ucs.amq.api;

import com.alibaba.fastjson.JSONObject;
import com.udfex.ucs.amq.api.service.ProducerService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.SessionAwareMessageListener;

import javax.jms.*;
import java.util.Properties;
import java.util.UUID;

/**
 * Created by Jeng on 16/1/31.
 */
public class ProducerImpl implements Producer {

    private static final Logger logger = LoggerFactory.getLogger(ProducerImpl.class);

    private ProducerService producerService;
    private JmsTemplate jmsTemplate;
    private ActiveMQConnectionFactory connectionFactory;
    private ActiveMQQueue destination;
    private ActiveMQQueue responseQueueDestination;
    private DefaultMessageListenerContainer messageListenerContainer;

    public ProducerImpl(ActiveMQConnectionFactory activeMQConnectionFactory, ProducerService producerHandler, Properties properties) {
        connectionFactory = activeMQConnectionFactory;
        String queueName = (String) properties.get(ProducerConst.TOPIC);
        String responseQueueName = queueName + ".response";
        destination = new ActiveMQQueue(queueName);
        responseQueueDestination = new ActiveMQQueue(responseQueueName);
        producerService = producerHandler;
        jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(connectionFactory);
        jmsTemplate.setDefaultDestination(destination);
        jmsTemplate.setSessionAcknowledgeMode(ActiveMQSession.INDIVIDUAL_ACKNOWLEDGE);
        jmsTemplate.setSessionTransacted(false);
        messageListenerContainer = new DefaultMessageListenerContainer();
        messageListenerContainer.setSessionTransacted(false);
        messageListenerContainer.setSessionAcknowledgeMode(4);
        messageListenerContainer.setConnectionFactory(connectionFactory);
        messageListenerContainer.setDestination(responseQueueDestination);
        messageListenerContainer.setMessageListener(new SessionAwareMessageListener<TextMessage>() {
            @Override
            public void onMessage(TextMessage textMessage, Session session) throws JMSException {
                String messageId = textMessage.getStringProperty(BaseMessage.MESSAGE_ID);
                String requestId = textMessage.getStringProperty(BaseMessage.REQUEST_ID);
                String message = textMessage.getText();
                logger.debug("Producer MessageListener onMessage : " + message);
                try {
                    ConsumeResult consumeResult = JSONObject.parseObject(message, ConsumeResult.class);
                    if (ConsumeResult.CONSUME_SUCCESS_CODE == consumeResult.getResultCode()) {
                        if(producerService != null){
                            producerService.onHandlerSuccess(messageId, requestId, message);
                        }
                    }else{
                        if(producerService != null){
                            producerService.onHandlerFailure(messageId, requestId, message);
                        }
                    }
                    textMessage.acknowledge();
                } catch (Exception e){
                    e.printStackTrace();
                    logger.error(e.getMessage(), e);
                }
            }
        });
        messageListenerContainer.initialize();
        messageListenerContainer.start();
    }

    public SendResult send(DataMessage dataMessage) {
        SendResult sendResult = new SendResult();
        // 发送前持久化消息数据，调用消息持久化接口
        if(dataMessage.getMessageId() == null){
            dataMessage.setMessageId(UUID.randomUUID().toString().replaceAll("\\-",""));
        }
        sendResult.setMessageId(dataMessage.getMessageId());
        final String messageId = dataMessage.getMessageId();
        final String requestId = dataMessage.getRequestId();
        final String msg = dataMessage.getMessageBody();
        try {
            jmsTemplate.send(destination, new MessageCreator() {
                public Message createMessage(Session session) throws JMSException {
                    TextMessage message = session.createTextMessage(msg);
                    message.setStringProperty(BaseMessage.MESSAGE_ID, messageId);
                    message.setStringProperty(BaseMessage.REQUEST_ID, requestId);
                    message.setJMSDeliveryMode(DeliveryMode.PERSISTENT);
                    message.setJMSReplyTo(responseQueueDestination);
                    return message;
                }
            });
            if(producerService != null){
                producerService.onSendSuccess(sendResult.getMessageId());
            }
            sendResult.setResultCode(SendResult.SEND_SUCCESS);
            return sendResult;
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            sendResult.setResultCode(SendResult.SEND_FAILURE);
            sendResult.setResultMsg(e.getMessage());
            if(producerService != null){
                producerService.onSendFailure(sendResult.getMessageId(), e.getMessage());
            }
            return sendResult;
        }
    }

    public void start() {
        //messageListenerContainer.initialize();
    }

    public void shutdown() {
        messageListenerContainer.shutdown();
    }
}
