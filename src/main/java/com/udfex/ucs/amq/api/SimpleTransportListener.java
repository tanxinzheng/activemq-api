package com.udfex.ucs.amq.api;

import org.apache.activemq.transport.TransportListener;

import java.io.IOException;

/**
 * Created by Jeng on 16/1/31.
 */
public class SimpleTransportListener implements TransportListener {

    @Override
    public void onCommand(Object o) {

    }

    @Override
    public void onException(IOException e) {

    }

    @Override
    public void transportInterupted() {
        System.out.println("===>> 断开");
    }

    @Override
    public void transportResumed() {
        System.out.println("===>> 重连");
    }
}
