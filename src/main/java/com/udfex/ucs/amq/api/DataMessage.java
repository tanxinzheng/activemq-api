package com.udfex.ucs.amq.api;


import java.io.Serializable;

/**
 * Created by Jeng on 2016/1/12.
 */
public class DataMessage extends BaseMessage implements Serializable {

    private String topic;
    private Object data;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
