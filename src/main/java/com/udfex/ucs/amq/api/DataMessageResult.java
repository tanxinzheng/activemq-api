package com.udfex.ucs.amq.api;

import java.util.Date;

/**
 * Created by Jeng on 2016/1/12.
 */
public class DataMessageResult {

    private String requestId;
    private String messageId;
    private Integer status;// 1：成功，-1：失败
    private String message;
    private Date responseTime;

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
