package com.udfex.ucs.amq.api.service;

import com.udfex.ucs.amq.api.DataMessage;
import com.udfex.ucs.amq.api.DataMessageResult;

/**
 * Created by Jeng on 2016/1/14.
 */
public interface ConsumerService {

    /**
     * 消息消费成功
     * @param dataMessageRequest
     */
    public void onHandlerSuccess(DataMessage dataMessageRequest, DataMessageResult dataMessageResult);

    /**
     * 消息消费失败
     * @param dataMessageRequest
     * @param dataMessageResult
     */
    public void onHandlerFailure(DataMessage dataMessageRequest, DataMessageResult dataMessageResult);

    public void onSendSuccess(DataMessage dataMessageRequest, DataMessageResult dataMessageResult);

    public void onSendFailure(DataMessage dataMessageRequest, DataMessageResult dataMessageResult);

}
