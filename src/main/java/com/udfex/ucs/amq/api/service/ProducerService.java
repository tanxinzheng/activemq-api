package com.udfex.ucs.amq.api.service;

import com.udfex.ucs.amq.api.DataMessage;

/**
 * Created by Jeng on 2016/1/12.
 */
public interface ProducerService {

    /**
     * 记录消息发送请求
     * @param message
     */
    public String saveMessage(DataMessage message);

    /**
     * 消息发送成功回调
     * @param dataMsgSendResult
     */
    public void onSendSuccess(String messageId);

    /**
     * 消息发送失败回调
     * @param dataMsgSendResult
     */
    public void onSendFailure(String messageId, String message);

    /**
     * 消息处理成功回调
     * @param dataMsgSendResult
     */
    public void onHandlerSuccess(String messageId, String requestId, String message);

    /**
     * 消息处理失败回调
     * @param dataMsgSendResult
     */
    public void onHandlerFailure(String messageId, String requestId, String message);
}
