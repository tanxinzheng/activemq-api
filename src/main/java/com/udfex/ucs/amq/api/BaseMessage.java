package com.udfex.ucs.amq.api;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jeng on 2016/1/14.
 */
public abstract class BaseMessage implements Serializable {

    // 消费成功信息
    public static final String REQUEST_ID = "REQUEST_ID";
    public static final String MESSAGE_ID = "MESSAGE_ID";

    private Date requestTime;
    private String requestId;
    private String messageId;
    private String messageBody;

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}
