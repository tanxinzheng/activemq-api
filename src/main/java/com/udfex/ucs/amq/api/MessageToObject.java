package com.udfex.ucs.amq.api;

/**
 * Created by Jeng on 2016/3/11.
 */
public interface MessageToObject<T extends BaseMessage> {

    public T fromMessage(DataMessage dataMessage);
}
