package com.udfex.ucs.amq.api;

import com.udfex.ucs.amq.api.service.ProducerService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Jeng on 2016/1/12.
 */
public class IAMQFactory {

    private static Map<String, Producer> producerMap = new HashMap<String, Producer>();
    private static Map<String, Consumer> consumerMap = new HashMap<String, Consumer>();

    public IAMQFactory(ActiveMQConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    private static ActiveMQConnectionFactory connectionFactory;

    private ProducerService producerService;

    public ProducerService getProducerService() {
        return producerService;
    }

    public void setProducerService(ProducerService producerService) {
        this.producerService = producerService;
    }

    public ActiveMQConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    public void setConnectionFactory(ActiveMQConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public static Producer createProducer(Properties properties){
        String queueName = properties.getProperty(ProducerConst.TOPIC);
        ActiveMQQueue destination = new ActiveMQQueue(queueName);
        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(connectionFactory);
        jmsTemplate.setDefaultDestination(destination);
        jmsTemplate.setSessionAcknowledgeMode(ActiveMQSession.INDIVIDUAL_ACKNOWLEDGE);
        jmsTemplate.setSessionTransacted(false);
        Producer producer = producerMap.get(queueName);
        if(producer == null){
            producer = new ProducerImpl(connectionFactory, null, properties);
            producerMap.put(queueName, producer);
        }
        return producer;
    }

    public static Producer createProducer(Properties properties, ProducerService producerService){
        String queueName = properties.getProperty(ProducerConst.TOPIC);
        ActiveMQQueue destination = new ActiveMQQueue(queueName);
        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(connectionFactory);
        jmsTemplate.setDefaultDestination(destination);
        jmsTemplate.setSessionAcknowledgeMode(ActiveMQSession.INDIVIDUAL_ACKNOWLEDGE);
        jmsTemplate.setSessionTransacted(false);
        Producer producer = producerMap.get(queueName);
        if(producer == null){
            producer = new ProducerImpl(connectionFactory,producerService, properties);
            producerMap.put(queueName, producer);
        }
        return producer;
    }

    public static Consumer createConsumer(Properties properties){
        DefaultMessageListenerContainer messageListenerContainer = new DefaultMessageListenerContainer();
        messageListenerContainer.setSessionTransacted(false);
        messageListenerContainer.setSessionAcknowledgeMode(4);
        messageListenerContainer.setConnectionFactory(connectionFactory);
        ConsumerImpl consumer = new ConsumerImpl();
        consumer.setMessageListenerContainer(messageListenerContainer);
        return consumer;
    }

}
