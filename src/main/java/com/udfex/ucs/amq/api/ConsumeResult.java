package com.udfex.ucs.amq.api;

import java.io.Serializable;

/**
 * Created by Jeng on 2016/3/11.
 */
public class ConsumeResult implements Serializable{

    public static final int CONSUME_SUCCESS_CODE = 1;
    public static final int CONSUME_FAIL_CODE = 0;
    public static final String CONSUME_SUCCESS_DEFAULT_MESSAGE = "SUCCESS";

    private int resultCode;
    private String resultMsg;
    private String messageId;
    private String requestId;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
